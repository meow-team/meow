#ifndef VERSION_H
#define VERSION_H

#define VERSION_RC  0,1,0,0

#ifdef _WIN32
    #define BINARY_RC   "Meow.exe"
#endif

#define PRODUCT_RC  "Meow Browser"
#define AUTHOR_RC   "Mist Poryvaev"

#endif // VERSION_H
